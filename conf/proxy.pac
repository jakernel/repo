function FindProxyForURL(url, host) {
    if (shExpMatch(host,"*.test.*")) {
        // 先通过第一个 socks5 连接，不行再通过第二个 proxy 连接，再不行就直接连接
        return "PROXY 192.168.3.7:8899;  DIRECT";
    }

    if (shExpMatch(host,"*.google.*") || shExpMatch(host,"*.github.*")) {
        // 先通过第一个 socks5 连接，不行再通过第二个 proxy 连接，再不行就直接连接
        return "PROXY 192.168.3.7:1080; PROXY 192.168.3.7:8899; DIRECT";
    }

    // 局域网内直接连接
    /*
    if (isInNet(host, "10.0.0.0", "255.0.0.0")){
        return "DIRECT";
    }
    */

    return "DIRECT";
}

