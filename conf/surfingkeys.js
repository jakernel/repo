// remap
map('p', 'cc');
map('F', 'cf');

// an example to remove mapkey `Ctrl-i`
// unmap('<Ctrl-i>');

addSearchAliasX('z', '知乎', 'https://www.zhihu.com/search?type=content&q=');
addSearchAliasX('t', '淘宝', 'http://s.taobao.com/search?q=');
addSearchAliasX('d', '京东', 'https://search.jd.com/Search?enc=utf-8&keyword=');
addSearchAliasX('f', '什么值得买', 'https://search.smzdm.com/?c=home&s=');
addSearchAliasX('y', '有道词典', 'http://dict.youdao.com/search?q=');
addSearchAliasX('i', 'IP查询', 'http://freeapi.ipip.net/');

// an example to create a new mapping `ctrl-y`
// mapkey('<Ctrl-y>', 'Show me the money', function() {
//      Front.showPopup('a well-known phrase uttered by characters in the 1996 film Jerry Maguire (Escape to close).');
//});

settings.historyMUOrder = false;
settings.interceptedErrors = ["*"];
// settings.stealFocusOnLoad = true;
settings.enableAutoFocus = false;

Front.registerInlineQuery({
    url: "http://fanyi.youdao.com/openapi.do?keyfrom=YouDaoCV&key=659600698&type=data&doctype=json&version=1.2&q=",
    parseResult: function(res) {
        try {
            res = JSON.parse(res.text);
            let exp = '';
            let pronunciations = [];
            if (res.basic && res.basic.explains) {
                for (let reg in res.basic.explains) {
                    pronunciations.push(`<div style='color:black;'>[${reg}] ${res.basic.explains[reg]}</div>`);
                }
            }
            if (res.web) {
                pronunciations.push(`<br><div style='color:black;'>网络释义</div>`);
                for (let reg in res.web) {
                    let val = (res.web[reg].value || []).join('\n');
                    pronunciations.push(`<div style='color:black;'>[${res.web[reg].key}] ${val}</div>`);
                }
            }
            exp = `${pronunciations.join("")}`;

            return exp;
        } catch (e) {
            return "";
        }
    }
});

// click `Save` button to make above settings to take effect.

